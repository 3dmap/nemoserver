const Bebel = require('bebel')
const directory = 'root'
const port = 8000

new Bebel({ directory })
  .listen(port)
  .start()
