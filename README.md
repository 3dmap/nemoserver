# Nemoserver

`Nemoserver` a simple newsgroups server based on JNTP protocol.

## Installation

This is a [Node.js](https://nodejs.org/en/) module available through the
[npm registry](https://www.npmjs.com/).

Before installing, [download and install Node.js](https://nodejs.org/en/download/).

```bash
$ npm i nemoserver
```

## License

  [MIT](LICENSE)